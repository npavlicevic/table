#
# Makefile 
# hash table
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic
OBJ_FLAGS=-c -static -g
AR_FLAGS=rcs
LIBS=-lm

FILES=table.h table.c table_test.h table_test.c table_main.c
CLEAN=table_main

all: main obj lib

main: ${FILES}
	${CC} ${CFLAGS} ${LIBS} table.c table_test.c table_main.c -o table_main

obj: ${FILES}
	${CC} ${OBJ_FLAGS} table.c -o table.o

lib: ${FILES}
	${AR} ${AR_FLAGS} libtable.a table.o

clean: ${CLEAN}
	rm $^
