#ifndef TABLE_TEST_H
#define TABLE_TEST_H
#include <assert.h>
#include "table.h"
typedef struct test {
  int value;
} Test;

Test *test_create(int value);
void test_destroy(Test **this);
void test_table_destroy(Table *table);
void test_insert_small();
#endif
